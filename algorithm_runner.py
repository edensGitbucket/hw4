
class AlgorithmRunner:

    def __init__(self, algorithm):
        self.algorithm = algorithm

    def fit(self, train_data, labels):
        self.algorithm.fit(train_data, labels)

    def predict(self, test_data, label):
        predicted_labels = self.algorithm.predict(test_data)

        # prints statistics
        total_samples = len(label)
        true_positive = 0
        false_positive = 0
        false_negative = 0
        true_negative = 0

        labels = list(label)
        # true means the movie succeeded- label is bigger or equal to 7
        for i in range(total_samples):
            if predicted_labels[i] == 1:
                if labels[i] >= 1:
                    true_positive += 1
                else:
                    false_negative += 1
            else:
                if labels[i] == 1:
                    false_positive += 1
                else:
                    true_negative += 1

        # # true means the movie succeeded- label is bigger or equal to 7
        # for i in range(total_samples):
        #     if predicted_labels[i] >= 7:
        #         if labels[i] >= 7:
        #             true_positive += 1
        #         else:
        #             false_positive += 1
        #     else:
        #         if labels[i] >= 7:
        #             false_negative += 1
        #         else:
        #             true_negative += 1

        # print("Accuracy is {}".format((true_positive + true_negative)/total_samples))
        # print("Precision is {}".format(true_positive / (true_positive + false_positive)))
        # print("Recall is {}".format(true_positive / (true_positive + false_negative)))

        accuracy = (true_positive + true_negative) / total_samples
        precision = true_positive / (true_positive + false_positive)
        recall = true_positive / (true_positive + false_negative)

        return accuracy, precision, recall
