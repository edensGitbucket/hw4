import pandas
from sklearn.model_selection import KFold


class Data:
    def __init__(self, path):
        self.df = pandas.read_csv(path)

    def preprocess(self):
        # print(self.df)

        # remove duplicate movies in rows.
        self.df.drop_duplicates(subset="movie_title", keep="first", inplace=True)
        # print(self.df)
        # print(self.df.columns)

        # remove unnecessary features for the training phase
        remove_features = ["content_rating", "movie_imdb_link", "plot_keywords", "movie_title"]
        self.df.drop(remove_features, axis=1, inplace=True)
        # print(self.df)
        # remove rows with empty cells
        self.df.dropna(inplace=True)
        # print(self.df)

        # normalize the numerical features
        numerical_features = ["actor_1_facebook_likes", "actor_2_facebook_likes", "actor_3_facebook_likes",
                              "aspect_ratio", "budget", "cast_total_facebook_likes", "director_facebook_likes",
                              "duration", "facenumber_in_poster", "gross", "movie_facebook_likes",
                              "num_critic_for_reviews", "num_user_for_reviews", "num_voted_users", "title_year"]
        for numerical_feature in numerical_features:
            self.df[numerical_feature] = (self.df[numerical_feature] - self.df[numerical_feature].mean()) / self.df[
                numerical_feature].std()
        # print(self.df)

        # convert the rest of the categorical features into indicators columns
        categorical_features = ["color", "country", "director_name", "language"]
        # for feature in categorical_features:
        #     indicator_array = self.df[feature].unique()
        #     for key in indicator_array:
        #         feature_column = self.df[feature]
        #         self.df[key] = [(1 if value == key else 0) for value in feature_column]
        # print(self.df)

        # # convert actors name to indicators
        # actors1 = set(self.df["actor_1_name"].unique())
        # actors2 = set(self.df["actor_2_name"].unique())
        # actors3 = set(self.df["actor_3_name"].unique())
        # actors = actors1.union(actors2).union(actors3)
        # self.df["actors"] = self.df["actor_1_name"] + self.df["actor_2_name"] + self.df["actor_3_name"]
        # for actor in actors:
        #     self.df[actor] = [(1 if actor in cell else 0) for cell in self.df["actors"]]
        genres = self.df.pop("genres").str.get_dummies(sep="|")
        self.df = pandas.concat([self.df, genres], axis=1)
        actors = (self.df.pop("actor_1_name")+"|"+self.df.pop("actor_2_name")+"|"+self.df.pop("actor_3_name")+"|").str.get_dummies(sep="|")
        self.df = pandas.concat([self.df, actors], axis=1)
        self.df = pandas.get_dummies(self.df, columns=categorical_features)

        # convert genres to indicators
        # genres_set = set()
        # for movie_genres in self.df["genres"]:
        #     for genre in movie_genres.split("|"):
        #         genres_set.add(genre)
        # print(genres_set)
        # for genre in genres_set:
        #     self.df[genre] = [(1 if genre in movie_genres else 0) for movie_genres in self.df["genres"]]
        #
        # categorical_features += ["genres", "actor_1_name", "actor_2_name", "actor_3_name", "actors"]
        # self.df.drop(categorical_features, axis=1, inplace=True)
        # print(self.df)

        # imdb score above 7 is good and under is bad
        score_column = self.df["imdb_score"]
        self.df.drop("imdb_score", axis=1, inplace=True)
        self.df["imdb_score"] = [(1 if score >= 7 else 0) for score in score_column]

        print(self.df.shape)


    def split_to_k_Folds(self):
        kfolds = KFold(n_splits=5, shuffle=False, random_state=None)
        return kfolds.split(self.df), 5


    def our_preprocess(self):
        print(self.df)

        # remove duplicate movies in rows.
        self.df.drop_duplicates(subset="movie_title", keep="first", inplace=True)
        print(self.df)
        print(self.df.columns)

        # remove unnecessary features for the training phase
        remove_features = ["content_rating", "movie_imdb_link", "plot_keywords", "movie_title"]
        self.df.drop(remove_features, axis=1, inplace=True)
        print(self.df)
        # remove rows with empty cells
        self.df.dropna(inplace=True)
        print(self.df)

        all_features = ["actor_1_facebook_likes", "actor_2_facebook_likes", "actor_3_facebook_likes",
                              "aspect_ratio",  "cast_total_facebook_likes", "director_facebook_likes",
                              "duration", "facenumber_in_poster", "movie_facebook_likes",
                              "num_critic_for_reviews", "num_user_for_reviews", "num_voted_users", "title_year",
                              "genres", "actor_1_name", "actor_2_name", "actor_3_name",
                        "color", "country", "director_name", "language"]
        self.df.drop(all_features, axis=1, inplace=True)
        score_column = self.df["imdb_score"]
        self.df["imdb_score"] = [(1 if score >= 7 else 0) for score in score_column]