import sys
import data
from algorithm_runner import AlgorithmRunner
from sklearn.neighbors import NearestCentroid
from sklearn.neighbors import KNeighborsClassifier


def run_algorithm(clf, folds, data, classifier):
    runner = AlgorithmRunner(clf)
    total_accuracy = 0
    total_precision = 0
    total_recall = 0

    for train_index, test_index in folds[0]:

        train_data = data.df.iloc[train_index, 0:-1].values
        labels = data.df.iloc[train_index, -1].values
        runner.fit(train_data, labels)

        test_data = data.df.iloc[test_index, 0:-1].values
        labels = data.df.iloc[test_index, -1].values
        accuracy, precision, recall = runner.predict(test_data, labels)

        total_accuracy += accuracy
        total_precision += precision
        total_recall += recall

    accuracy = total_accuracy / folds[1]
    precision = total_precision / folds[1]
    recall = total_recall / folds[1]

    print(classifier + ": {}, {}, {}".format(accuracy, precision, recall))


def main(argv):
    print("Question 1:")
    movies_data = data.Data(argv[1])
    movies_data.preprocess()
    folds = movies_data.split_to_k_Folds()
    clf = NearestCentroid()
    run_algorithm(clf, folds, movies_data, "KNN classifier")

    movies_data = data.Data(argv[1])
    movies_data.preprocess()
    folds = movies_data.split_to_k_Folds()
    clf = KNeighborsClassifier(n_neighbors=10)
    run_algorithm(clf, folds, movies_data, "Rocchio classifier")

    print("Question 2:")
    movies_data = data.Data(argv[1])
    movies_data.our_preprocess()
    folds = movies_data.split_to_k_Folds()
    clf = NearestCentroid()
    run_algorithm(clf, folds, movies_data, "KNN classifier")


if __name__ == "__main__":
    main(sys.argv)
